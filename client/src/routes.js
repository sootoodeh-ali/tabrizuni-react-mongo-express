import React , {Component} from 'react';
import { Route , Switch } from "react-router-dom";
import Home from './components/Home/home.js'
import FormLeft from './components/forms/formLeft.js'
import FormRight from './components/forms/formRight.js'
import Layout from './hoc/layout/layout.js';

class Routes extends Component {


    render(){
        return(
            <Layout>
            <Switch>
                <Route path='/' exact component={Home}/>
                <Route path='/form/right' exact component={FormRight}/>
                <Route path='/form/left' exact component={FormLeft}/>
            </Switch>
            </Layout>
        )
    }

}



export default Routes;