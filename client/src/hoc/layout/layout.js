import React , {Component} from 'react';

import './layout.css'
import Header from './../../components/header/header.js'
import Footer from './../../components/footer/footer.js'
class Layout extends Component{


    state={

        shownav : false

    }

    toggleSidenav = (action) => {
            this.setState({
                shownav : action
            })
    }


    render(){
        return(
            <div>
            
                        <Header
                        showNav = {this.state.shownav}
                        onHideNav = {() => {this.toggleSidenav(false)}}
                        onOpenNav = {() => {this.toggleSidenav(true)}}
                        />
                    {this.props.children}
                    
                        
                    <Footer/>
            </div>
        )
    }



}



export default Layout;