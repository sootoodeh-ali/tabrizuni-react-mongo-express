import React from 'react'
import style from './footer.css'
import { Link } from "react-router-dom";



const Footer = () => {
    return(
        <footer className={style.footer}>
                <Link to='/' className={style.logo}>
                <img alt='Logo' src='/images/logo/logo.png'/>
                </Link>
                <div className={style.right}>
                @تمامی حقوق محفوظ است
                </div>
        </footer>
    )
}




export default Footer;