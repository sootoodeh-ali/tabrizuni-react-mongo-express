import React from 'react';
import FontAwesome  from 'react-fontawesome'
import style from './sidenav.css'
import { Link } from "react-router-dom";

const SideNavItems = () => {


    return(
            <div>
        <div className={style.option}>
            <Link to='/'>
                <FontAwesome name='home'/>
                خانه
            </Link>
        </div>
        <div className={style.option}>
            <Link to='/'>
                <FontAwesome name='sign-in'/>
                ورود
            </Link>
        </div>
        <div className={style.option}>
            <Link to='/'>
                <FontAwesome name='qrcode'/>
                درباره ما 
            </Link>
        </div>
            </div>

        
    )

}


export default SideNavItems;