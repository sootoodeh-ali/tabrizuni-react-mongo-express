import React from 'react';
import SideNav from 'react-simple-sidenav'
import SideNavItems from './side_navitems.js'


const SideNavigation = (props) => {
   
            return(
                   
                <div >

                <SideNav 
               navStyle={{
                         background :'#18181a',
                         maxWidth : '250px'
                       
                }}        
                titleStyle={{
                    background:'dark',
                    color :'dark'
                    
                }}
                    
                showNav = {props.showNav}
                onHideNav={props.onHideNav}
                openFromRight={true}
                >
                
                    <SideNavItems/>
                </SideNav>
            </div>               
              )  }
       
      
    




export default SideNavigation;
