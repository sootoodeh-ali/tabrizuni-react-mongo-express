import React from 'react';
import style from './header.css'
import { Link } from "react-router-dom";
import FontAwesome from 'react-fontawesome'
import SideNav from './sideNav/sideNav.js'


const Header = (props) => {

const logo = () =>{

    return(
        <Link to='/' className={style.logo}>
        <img alt='Logo' src='/images/logo/logo.png'/>
        </Link>
    )

    
}
const navBars = () =>{
    return(
        <div className={style.Bars}>
                <FontAwesome name='bars'
                onClick={props.onOpenNav}
                
                 />
        </div>
    )
}

const searchInput = () => {
    return(
        <div className={style.divSearch}>
        <span>
            <FontAwesome name='search'
                className={style.searchIcon}
            />
        </span>
        <div className={style.field}>
            <input type='search' placeholder='Search...'/>
        </div>
       </div>

    )
}
const headerText = () => {
    return(
        
        <div className={style.headText}>
            دانشگاه تبریز
        </div>
      
    )
}



    return(
        <header className={style.header}>
        <SideNav {...props}/>
            <div className={style.headeropt}>
               {navBars()}
               {logo()}
               {headerText()}
                 {searchInput()} 
               
            </div>
        </header>

    )
}



export default Header ;