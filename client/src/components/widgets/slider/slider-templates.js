import React from 'react';
import Slick from 'react-slick'

  
import style from './slider.css';


const SliderTemplates = (props) => {


        const settings = {
            dots : true,
            infinite:true,
            arrows:false,
            speed : 500,
            SlidesToShow : 1,
            slidesToScroll:1,
            

        }


    return (
            <Slick {...settings} >
                <div>
                    <div className={style.slider_item}>
                        <div className={style.slider_img}
                            style={{
                                background : `url(../images/slider/1.jpg)`
                            }}
                            >
                        
                        </div>
                    </div>    
                </div>
                <div>
                    <div  className={style.slider_item}>
                        <div className={style.slider_img}
                        style={{
                                background : `url(../images/slider/4.jpg)`
                            }}>
                        
                        </div>
                    </div> 
                </div>
                <div>
                    <div className={style.slider_item} >
                        <div className={style.slider_img}
                        style={{
                                background : `url(../images/slider/3.jpg)`
                            }}>

                        </div>
                    </div>    
                </div>
            </Slick>
    )
} 



export default SliderTemplates;