import React from 'react';
import CardNew from '../../hoc/layout/cards/cardnew.js'
import style from './home.css'
import  FontAwesome  from 'react-fontawesome'
const Home = () =>{
    return(
    <div>
    <div className={style.homeTextContainer}>
    <div className={style.homeText}>
        
            خوش آمدید.  
     </div>
     <div className={style.secHomeText}>
     برای عضویت یکی از بخش های زیر را انتخاب نمایید.
     </div>

     <div className={style.homePic}>

            <FontAwesome name='arrow-down'/>

     </div>


     </div>
     
            <CardNew/>
    </div>
    )
}


export default Home;