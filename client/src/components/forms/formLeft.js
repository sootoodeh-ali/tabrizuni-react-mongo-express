import React , {Component} from 'react';
import style from './form.css';
   
const formValid = ({formErrors,...rest}) =>{
   let valid = true;

   Object.values(formErrors).forEach( val => {
      val.length > 0 && (valid = false)
   })
   console.log(rest);
   

   Object.values(rest).forEach( val => {
      val === null && (valid = false)
   } )


return valid;

}
const emailRegex = RegExp(
   /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
)




class FormLeft extends Component{
         constructor(props){
               super(props);
               this.state= {
                     firstName:null,
                     lastName:null,
                     email:null,
                     age:null,
                     formErrors : {
                        firstName:"",
                     lastName:"",
                     email:"",
                     age:"",
                     code:""
                     }
               };
         };
        
handleChange = e => {
   const { name , value } = e.target;
   let formErrors = this.state.formErrors;




   switch (name) {
      case "firstName":
         formErrors.firstName = value.length<3   ? "نام وارد شده مناسب نیست" : ""
         break;
         case "lastName":
         formErrors.lastName = value.length<3   ? "نام خانوادگی وارد شده مناسب نیست" : ""
         break;
         case "email":
         formErrors.email = !emailRegex.test(value)   ? "ایمیل صحیح وارد نشده است" : ""
         break;
         case "age":
         formErrors.age = value.length!==2   ? "سن وارد شده درست نیست" : ""
         break;
         case "code":
         formErrors.code = value.length!==10   ? "کد ملی شما درست نمی باشد" : ""
         break;
      default:
         break;
   }
   this.setState({formErrors,[name] : value })
}

        render(){
           const {formErrors} = this.state;
           return(

            <div className={style.wrapper}>
            <div className={style.formwrapper}>
            <h1>فرم عضویت</h1>
            <form method='POST' action='http://localhost:3001/api/oldUsers'>
                <div className={style.firstName}>
                <label htmlFor='firstName'>نام</label>
                <input type='text'

                  placeholder="نام خود را وارد کنید" 
                  name='firstName'
                   onChange={this.handleChange}
                   />
                   {
                      formErrors.firstName.length>0 && ( 
                         <span className={style.errorMessage}>{formErrors.firstName}</span>
                      )
                   }
                </div>
                <div className={style.lastName}>
                <label htmlFor='lastName'>نام خانوادگی</label>
                <input type='text'
                  placeholder="نام خانوادگی خود را وارد کنید" 
                  name='lastName'
                   onChange={this.handleChange}
                   />
                   {
                     formErrors.lastName.length>0 && ( 
                        <span className={style.errorMessage}>{formErrors.lastName}</span>
                     )
                  }
                   </div>
                   <div className={style.email}>
                   <label htmlFor='email'>ایمیل</label>
                   <input type='email'
                     placeholder='ایمیل خود را وارد کنید' 
                     name='email'
                      onChange={this.handleChange}
                      />
                      {
                        formErrors.email.length>0 && ( 
                           <span className={style.errorMessage}>{formErrors.email}</span>
                        )
                     }
                      </div>
                      <div className={style.password}>
                      <label htmlFor='age'>سن</label>
                      <input type='number'
                        placeholder=" سن خود را وارد کنید" 
                        name='age'
                         onChange={this.handleChange}
                         />
                         {
                           formErrors.age.length>0 && ( 
                              <span className={style.errorMessage}>{formErrors.age}</span>
                           )
                        }
                         </div>
                         <div className={style.password}>
                      <label htmlFor='code'>کد ملی</label>
                      <input type='number'
                        placeholder=" کد ملی خود را وارد کنید" 
                        name='code'
                         onChange={this.handleChange}
                         />
                         {
                           formErrors.age.length>0 && ( 
                              <span className={style.errorMessage}>{formErrors.code}</span>
                           )
                        }
                         </div>
                   
            <div className={style.submit}>
            
            <button type='submit'>ارسال</button>
            <small></small>
            </div>
            
            </form>
            </div>            
            </div>

           )
        }
}


export default FormLeft;