import React,{Component} from 'react';
import style from './form.css'
import"./form.css";

   
const formValid = ({formErrors,...rest}) =>{
   let valid = true;

   Object.values(formErrors).forEach( val => {
      val.length > 0 && (valid = false)
   })
   console.log(rest);
   

   Object.values(rest).forEach( val => {
      val === null && (valid = false)
   } )


return valid;

}
const emailRegex = RegExp(
   /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
)




class FormRight extends Component{
         constructor(props){
               super(props);
               this.state= {                    
                     userName:null,
                     password:null,
                     formErrors : {
                        userName:"",
                     password:""
                     }
               };
         };
        
handleChange = e => {
   const { name , value } = e.target;
   let formErrors = this.state.formErrors;




   switch (name) {
         case "userName":
         formErrors.userName = value.length!==10  ? "نام کاربری صحیح وارد نشده است" : ""
         break;
         case "password":
         formErrors.password = value.length!==9   ? "رمز عبوراشتباه است" : ""
         break;
      default:
         break;
   }
   this.setState({formErrors,[name] : value })
}


        render(){
           const {formErrors} = this.state;
           return(

            <div className={style.wrapper}>
            <div className={style.formwrapper}>
            <h1>فرم عضویت</h1>
            <form method='POST' action='http://localhost:3001/api/newUsers'>

                   <div className={style.email}>
                   <label htmlFor='email'>نام کاربری</label>
                   <input type='number'
                     placeholder='نام کاربری خود را وارد کنید' 
                     name='userName'
                      onChange={this.handleChange}
                      />
                      {
                        formErrors.userName.length>0 && ( 
                           <span className={style.errorMessage}>{formErrors.userName}</span>
                        )
                     }
                      </div>
                      <div className={style.password}>
                      <label htmlFor='password'>رمز عبور</label>
                      <input type='password'
                        placeholder="رمز عبور خود را وارد کنید" 
                        name='password'
                         onChange={this.handleChange}
                         />
                         {
                           formErrors.password.length>0 && ( 
                              <span className={style.errorMessage}>{formErrors.password}</span>
                           )
                        }
                         </div>
                   
            <div className={style.submit}>
            
            <button type='submit'>ارسال</button>
            <small> کد ملی شما به عنوان نام کاربری تنظیم شده است</small>
            <small> از شماره دانشجویی خود به عنوان رمز استفاده کنید </small>
            </div>
            
            </form>
            </div>            
            </div>

           )
        }
}

export default FormRight;
