const mongoose = require('mongoose');

mongoose.Promise = global.Promise;


const oldUserSchema = mongoose.Schema({
    name : {
        type:String,
        trim:true,
        required:true,
        unique:1,
    },
    lastName : {
        type : String,
        trim:true,
        required:true,
        unique:1,
    },
    email:{
        type : String,
        unique:1,
        trim:true,
        required:true
    },
    age:Number,
    code:Number
})



const OldUser = mongoose.model('OldUser',oldUserSchema);

module.exports = {OldUser}