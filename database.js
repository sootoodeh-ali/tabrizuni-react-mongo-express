const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/newUsers', { useNewUrlParser: true })


const newUserSchema = mongoose.Schema({
    name : String,
    lastName : String,
    email:String,
    age:Number,
    password:Number,
    educationalCode:Number
})



const NewUser = mongoose.model('NewUser',newUserSchema);


// const addNewUser = new NewUser({
//     name : "رضا",
//     lastName : "جوان",
//     email:"javan@gmail.com",
//     age:26,
//     password:1478520369,
//     educationalCode:312034567
// }) 


// addNewUser.save((err,doc) => {
//     if(err) return console.log(err);
//     console.log(doc);
    
// })

module.exports = {NewUser}